(ns app
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            ["material-react-table" :as rt :default MaterialReactTable]))

(defn main []
  (rdom/render
   [:> MaterialReactTable
    {:data
     [{:COUNTRY "US" :price 1}
      {:COUNTRY "US" :price 2}
      {:COUNTRY "US" :price 3}
      {:COUNTRY "VN" :price 5}
      {:COUNTRY "VN" :price 5}]
     :columns
     [{:header "Country" :accessorKey :COUNTRY :size 125}
      {:header "Price" :accessorKey :price :size 65
       :aggregationFn "sum" :AggregatedCell (fn [cell table] "nm")}]
     :enableGrouping true
     :initialState #js {:grouping #js ["COUNTRY"]}}]
   (js/document.getElementById "root")))
