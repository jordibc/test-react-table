# Test of react table

This is a little program to test the issue found in
https://clojurians.zulipchat.com/#narrow/stream/151762-clojurescript/topic/material-react-table.20behaviour

After cloning this repository, I do:

```sh
yarn install
```

and then I open `src/app.cljs` in emacs and run `cider-jack-in-cljs`
(with "shadow" repl, and selecting "app").

At this point I get a compilation error related to a file
(`remove-accents.js`) in a js module (`@tanstack/match-sorter-utils`):

```
[:app] Configuring build.
[:app] Compiling ...
[:app] Build failure:
Failed to inspect file
  /data/projects/personal/2023/test-react-table/node_modules/@tanstack/match-sorter-utils/build/lib/remove-accents.js

it was required from
  /data/projects/personal/2023/test-react-table/node_modules/@tanstack/match-sorter-utils/build/lib/index.js

Errors encountered while trying to parse file
  /data/projects/personal/2023/test-react-table/node_modules/@tanstack/match-sorter-utils/build/lib/remove-accents.js
  {:line 131, :column 3, :message "Character '̆' (U+0306) is not a valid identifier start char"}
```

My unelegant solution is to edit that file and leave `characterMap`
empty:

```js
const characterMap = {};
```

Then, I run again `cider-jack-in-cljs` and the app run flawlessly for
me.
